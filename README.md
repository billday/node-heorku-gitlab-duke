# node-heroku-dpl

poc deploy node https://medium.com/@seulkiro/deploy-node-js-app-with-gitlab-ci-cd-214d12bfeeb5

The code at above website worked with gitlab.com to auto-deploy to heorku with ci/cd, but on gitlab.oit.duke.edu the ci/cd jobs were failing with ```npm not found``` error. I think this error was because it was using any-old shared runner and/or (docker?) image without npm. This may have been fixed by adding a tag ``` oit-shared``` in gitlab-ci.yml so that a shared runner with that tag is chosen, with the hope that the ```oit-shared``` runner has npm. The job than suceeded and deployed to heoroku ok at least once ;-) 

In general, still unsure of how to tell what runners do what, and how to select suitable shared runner with tag(s). For example, I had tried the tag ```docker``` as suggested on internet, but gitlab.oit.duke.edu has no shared runners tagged ```docker``` but job still failed until I changed the ```docker``` tag to ```oit-shared``` as shown below.
```
production:
  tags:
    - oit-shared
  type: deploy
  type: deploy
  stage: production
  stage: production
  image: ruby:latest
  image: ruby:latest
  ```